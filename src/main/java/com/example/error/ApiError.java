package com.example.error;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Date;
import java.util.Map;

@Data
public class ApiError {
    private static ApiError apiError;
    private int status;
    private String message;
    private String path;
    private Map<String,String> validationErrors;

    private ApiError(int status,String message,String path){
        this.status=status;
        this.message=message;
        this.path=path;
    }

    public static ApiError getInstance(int status,String message,String path){

        if (apiError==null)
            return new ApiError(status,message,path);
        else
            return apiError;
    }

}
