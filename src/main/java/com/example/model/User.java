package com.example.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @NotNull
    @Size(min = 4)
    @Column(unique=true)
    private String username;
    @NotNull
    @Size(min = 4)
    private String displayname;
    @NotNull
    private String password;


}
